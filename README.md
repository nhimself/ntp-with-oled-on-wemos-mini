# Wemos NTP time server with OLED Display 

This fun project brings a out of the box for having a digital clock displayed on your OLED expansion board. 
There is nothing special used in this project besides the SparkFUN lib for OLED.

# Contents
- Versions (#versions)
- Wireing (#wireing)
- Bugs (#bugs)
- Licens and credits (#licens-and-credits)


### Versions

- v1.0 Initial build 


### Wirering

There is no wirering for this ino all development have been done with a Wemos and OLED expansion board

### Bugs

Found a bug?
I would appreciate if you took the time and registered it at bitbucket or wrote me an email.

Please register it at:
[Bitbucket](https://bitbucket.org/nhimself/ntp-with-oled-on-wemos-mini/issues)

### Licens and Credits


Copyright 2017 Niki Mench

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

--- 
Arduino IDE is developed and maintained by the Arduino team. The IDE is licensed under GPL.

ESP8266 core includes an xtensa gcc toolchain, which is also under GPL.

Espressif SDK included in this build is under Espressif MIT License.

ESP8266 core files are licensed under LGPL.

---