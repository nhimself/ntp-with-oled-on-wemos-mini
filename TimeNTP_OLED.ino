/* Sketch made by Nhimself
 * email: nikimench@gmail.com
 * source: https://bitbucket.org/nhimself/ntp-with-oled-on-wemos-mini
 * This sketch is licensed under MIT License.
 * 
 * 
 * This sketch uses the below library's please make sure that these are installed before continuing.

 */
#include <TimeLib.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <Wire.h>
#include <SFE_MicroOLED.h>

//Display pins
#define PIN_RESET 255                  // Reset pin
#define DC_JUMPER 0                    // I2C Addres: 0 - 0x3C, 1 - 0x3D
MicroOLED oled(PIN_RESET, DC_JUMPER);  // I2C Example

/* Remeber to fill out the below information
 *
 * Networks settings
 */ 
const char ssid[] = ".........";       //  your network SSID (name)
const char pass[] = ".........";       // your network password

// NTP Servers:
static const char ntpServerName[] = "0.dk.pool.ntp.org";
//static const char ntpServerName[] = "dk.pool.ntp.org";

// Timezone setting
const int timeZone = 1;                // Central European Time
//const int timeZone = -5;             // Eastern Standard Time (USA)
//const int timeZone = -4;             // Eastern Daylight Time (USA)
//const int timeZone = -8;             // Pacific Standard Time (USA)
//const int timeZone = -7;             // Pacific Daylight Time (USA)

// Init Wifi
WiFiUDP Udp;
unsigned int localPort = 2390;         // local port to listen for UDP packets

//Get first time from NTP
time_t getNtpTime();
//void digitalClockDisplay();
//void printDigits(int digits);
//void sendNTPpacket(IPAddress &address);

// Init OLED
void initOLED();

void setup()
{
  Serial.begin(115200); // Wemos runs 115200 but remeber to set your serial panel
  Serial.println("TimeNTP Example");
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  oled.print("Connecting to ");
  oled.println(ssid);
  oled.display();
  WiFi.begin(ssid, pass);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    oled.print(".");
    oled.display();
  }
  oled.clear(PAGE);
  oled.clear(ALL);
  oled.setCursor(0, 0);
  
  Serial.print("IP: ");
  Serial.println(WiFi.localIP());
  Serial.println("Starting UDP");
  
  Udp.begin(localPort);
  
  Serial.print("Local port:");
  Serial.print(Udp.localPort());
  Serial.println("waiting for sync");
  
  setSyncProvider(getNtpTime);
  setSyncInterval(60000);
}

time_t prevDisplay = 0;             // When the digital clock was displayed

void loop()
{
  oled.setCursor(0, 30);            // Set cursor to 30 pixels down (line 3)
  ipDisplay();                      // Display IP and port
  oled.display();                   // Write to display
  
  if (timeStatus() != timeNotSet) { // Check if time is set
    if (now() != prevDisplay) {     // Check if time has changed, and update display only if time has changed
      prevDisplay = now();
      while (prevDisplay = now()){
        oled.setCursor(0, 0);       // Reset cursor 
        digitalClockDisplay();      // Update time on display
        delay(1000);                // Update display every second
        oled.setCursor(0, 10);
        digitalCalendarDisplay();   // Update date on display
      }
    }
  }
}

/*
 * Time
 */
void digitalClockDisplay()
{
  Serial.println("Writing time to display");
  oled.print(hour());
  printDigits(minute());
  printDigits(second());
  oled.display();
}

/*
 * Date
 */
void digitalCalendarDisplay()
{
  oled.print(day());
  oled.print("/");
  oled.print(month());
  oled.print("/");
  oled.println(year());
  oled.println();
  oled.display();
}

/*
 * IP and port 
 */
void ipDisplay()
{
  oled.print(WiFi.localIP());
  oled.print("Port:");
  oled.println(Udp.localPort());
}

/*
 * Pre '0'
 */
void printDigits(int digits)
{
  // utility for digital clock display: prints preceding colon and leading 0
  oled.print(":");
  if (digits < 10)
    oled.print('0');
  oled.print(digits);
}

/*
 * Clear OLED
 */
void initOLED()
{
  // Init OLED and clear display
  oled.begin();     // Initialize the OLED
  oled.clear(PAGE); // Clear the display's internal memory
  oled.clear(ALL);
  oled.print("Booting...");
  oled.display();
} 

/*
 * NTP
 */
const int NTP_PACKET_SIZE = 48;     // NTP time is in the first 48 bytes of message
byte packetBuffer[NTP_PACKET_SIZE]; //buffer to hold incoming & outgoing packets

time_t getNtpTime()
{
  IPAddress ntpServerIP;            // NTP server's ip address
  while (Udp.parsePacket() > 0) ;   // discard any previously received packets
  Serial.println("Transmit NTP Request");
  
  // get a random server from the pool
  WiFi.hostByName(ntpServerName, ntpServerIP);
  Serial.print(ntpServerName);
  Serial.print(": ");
  Serial.println(ntpServerIP);
  sendNTPpacket(ntpServerIP);
  uint32_t beginWait = millis();
  while (millis() - beginWait < 1500) {
    int size = Udp.parsePacket();
    if (size >= NTP_PACKET_SIZE) {
      Serial.println("Receive NTP Response");
      Udp.read(packetBuffer, NTP_PACKET_SIZE);  // read packet into the buffer
      unsigned long secsSince1900;
      
      // convert four bytes starting at location 40 to a long integer
      secsSince1900 =  (unsigned long)packetBuffer[40] << 24;
      secsSince1900 |= (unsigned long)packetBuffer[41] << 16;
      secsSince1900 |= (unsigned long)packetBuffer[42] << 8;
      secsSince1900 |= (unsigned long)packetBuffer[43];
      return secsSince1900 - 2208988800UL + timeZone * SECS_PER_HOUR;
    }
  }
  Serial.println("No Response from NTP Server...");
  return 0; // return 0 if unable to get the time
}

// Send NTP request to time server at the given address
void sendNTPpacket(IPAddress &address)
{
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;            // Stratum, or type of clock
  packetBuffer[2] = 6;            // Polling Interval
  packetBuffer[3] = 0xEC;         // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12] = 49;
  packetBuffer[13] = 0x4E;
  packetBuffer[14] = 49;
  packetBuffer[15] = 52;
  // Send packet requesting time:
  Udp.beginPacket(address, 123); //NTP requests are to port 123
  Udp.write(packetBuffer, NTP_PACKET_SIZE);
  Udp.endPacket();
}
